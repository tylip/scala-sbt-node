ARG SCALA_SBT_VERSION
FROM sbtscala/scala-sbt:$SCALA_SBT_VERSION

ARG NODEJS_VERSION

RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get install nodejs=${NODEJS_VERSION}-deb-1nodesource1

ARG YARN_VERSION

RUN curl -sS http://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb http://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update
RUN apt-get -y install yarn=${YARN_VERSION}-1
