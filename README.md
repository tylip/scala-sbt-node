# scala-sbt-node

Docker image for Scala + sbt + Node.js

Published to Docker Hub: [**`tylip/scala-sbt-node`**](https://hub.docker.com/r/tylip/scala-sbt-node/)

Based on [`hseeberger/scala-sbt`](https://hub.docker.com/r/hseeberger/scala-sbt).
